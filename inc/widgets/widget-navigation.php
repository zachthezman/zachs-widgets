<?php
// copy the profile actions array to a php var
$nav_items = ( isset( $data->navigation->items ) ) ? $data->navigation->items : '';

// start output vars
$output = '';
$dropdown_output = '';

// nav items found
if( $nav_items ) {
	// start output
	$output .= '<nav class="main-nav mod--shadow">';
	$output .= '<ul class="main-nav__list mod--rounded">';

	// listing navigation
	foreach ( $nav_items as $item ) {
		// nav item : text
		$item_text = ( isset( $item->text ) ) ? $item->text : '';
		// nav item : link
		$item_link = ( isset( $item->link ) ) ? $item->link : '';
		// nav item : icon
		$item_icon = ( isset( $item->icon ) ) ? $item->icon : '';
		// nav item : alerts
		$item_alerts = ( isset( $item->alerts ) ) ? ' data-alert="' . $item->alerts . '"' : '';
		// nav item : dropdown
		$item_dropdown = ( isset( $item->dropdown ) ) ? $item->dropdown : '';

		// if nav item has dropdown
		$has_dropdown = ( $item_dropdown ) ? ' has--dropdown' : '';
		// if nav item has icon
		$icon = ( $item_icon ) ? '<i class="icon-' . $item_icon . '"></i> ' : '';

		// nav item output start
		$output .= '<li class="main-nav__list--item' . $has_dropdown . '"><a href="' . $item_link . '" class="main-nav__list--item__link">' . $icon . '<span' . $item_alerts . '>' . $item_text . '</span></a>';

		// if nav item dropdown found
		if( $item_dropdown ) {
			// dropdown output start
			$dropdown_output .= '<ul class="main-nav__dropdown">';

			// loop dropdown items
			foreach ( $item_dropdown as $drop_item ) {
				// dropdown nav item : text
				$drop_item_text = ( isset( $drop_item->text ) ) ? $drop_item->text : '';
				// dropdown nav item : link
				$drop_item_link = ( isset( $drop_item->link ) ) ? $drop_item->link : '';

				// dropdown nav item output
				$dropdown_output .= '<li class="main-nav__dropdown--item"><a href="' . $drop_item_link . '" class="main-nav__dropdown--item__link">' . $drop_item_text . '</a></li>';
			}
			// dropdown output end
			$dropdown_output .= '</ul>';
		}

		// include dropdown output
		$output .= $dropdown_output;

		// nav item output end
		$output .= '</li>';
	}

	// end output
	$output .= '</ul>';
	$output .= '</nav>';
}

// display output
echo $output;