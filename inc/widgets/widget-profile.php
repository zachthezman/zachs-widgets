<?php
// copy the profile array to a php var
$profile = ( isset( $data->profile ) ) ? $data->profile : '';
$profile_actions = ( isset( $data->profile->actions ) ) ? $data->profile->actions : '';

// start output vars
$output = '';
$actions_output = '';

// profile data found
if( $profile ) {
	// name
	$profile_name = ( isset( $data->profile->name ) ) ? $data->profile->name : '';
	// avatar
	$profile_avatar = ( isset( $data->profile->avatar ) ) ? $data->profile->avatar : '';
	// bio
	$profile_bio = ( isset( $data->profile->bio ) ) ? $data->profile->bio : '';

	// start profile output
	$output .= '<div class="profile mod--rounded mod--shadow">';
	$output .= '<div class="profile__user">';

	// if avatar found
	$output .= ( $profile_avatar ) ? '<div class="profile__user--avatar"><span class="profile__user--avatar__wrap"><img src="' . $profile_avatar . '" alt="' . $profile_name . '" title="' . $profile_name . '"></span></div>' : '';

	// if name or bio found
	if( $profile_name || $profile_bio ) {
		// continue profile output
		$output .= '<div class="profile__user--info">';

		// if name found
		$output .= ( $profile_name ) ? '<span class="profile__user--info__name">' . $profile_name . '</span>' : '';

		// if bio found
		$output .= ( $profile_bio ) ? '<span class="profile__user--info__bio">' . $profile_bio . '</span>' : '';

		// continue profile output
		$output .= '</div>';
	}
	// continue profile output
	$output .= '</div>';

	// if profile actions found
	if( $profile_actions ) {
		// actions output start
		$actions_output .= '<div class="profile__actions--wrap">';
		$actions_output .= '<ul class="profile__actions">';

		foreach ( $profile_actions as $action ) {
			// actions : text
			$action_text = ( isset( $action->text ) ) ? $action->text : '';
			// actions : link
			$action_link = ( isset( $action->link ) ) ? $action->link : '';
			// actions : icon
			$action_icon = ( isset( $action->icon ) ) ? $action->icon : '';
			// actions : current
			$action_current = ( isset( $action->current ) && $action->current === true ) ? ' current' : '';

			// if icon found
			$icon = ( $action_icon ) ? '<i class="icon-' . $action_icon . '"></i> ' : '';

			// actions item output
			$actions_output .= '<li class="profile__actions--item' . $action_current . '"><a class="profile__actions--item__link" href="' . $action_link . '">' . $icon . $action_text . '</a></li>';
		}
		// actions output end
		$actions_output .= '</ul>';
		$actions_output .= '</div>';
	}

	// include actions output
	$output .= $actions_output;

	// end profile output
	$output .= '</div>';
}

// display output
echo $output;