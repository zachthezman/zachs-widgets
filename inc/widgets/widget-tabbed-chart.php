<?php
// copy the charts array to a php var
$charts = ( isset( $data->tabbed_charts->charts ) ) ? $data->tabbed_charts->charts : '';

// start output vars
$output = '';
$tabs_output = '';
$tabs_content_output = '';

// set counts to 0
$tabcount = 0;
$stat_count = 0;

// charts found
if( $charts ) {
	// start output
	$output .= '<div class="tabbed-chart mod--rounded">';

	// start tabs output
	$tabs_output .= '<ul class="tabbed-chart__tabs">';

	// loop through charts
	foreach( $charts as $chart ) {
		// tab : text
		$tab_text = ( isset( $chart->tabtext ) ) ? $chart->tabtext : '';
		// tab : title
		$tab_title = ( isset( $chart->tabtitle ) ) ? $chart->tabtitle : '';

		// chart : type
		$chart_type = ( isset( $chart->options->type ) ) ? $chart->options->type : '';
		// chart : class
		$chart_class = ( isset( $chart->options->class ) ) ? $chart->options->class : '';
		// chart : more information
		$chart_moreinfo = ( isset( $chart->stats->moreinfo ) ) ? $chart->stats->moreinfo : '';
		// chart : stats
		$chart_stats = ( isset( $chart->stats->counts ) ) ? $chart->stats->counts : '';

		// set first tab to current
		$current = ( $tabcount === 0 ) ? ' current' : '';

		// show first tab, hide others
		$show_first = ( $tabcount === 0 ) ? ' mod--show' : ' mod--hide';

		// tabs
		$tabs_output .= '<li class="tabbed-chart__tabs--item' . $current . '" data-tab="' . $tab_text . '">' . $tab_text . '</li>';

		// start tab content output
		$tabs_content_output .= '<div data-tab-content="' . $tab_text . '" data-chart-type="' . $chart_type . '" class="tabbed-chart__content mod--rounded' . $show_first . '">';

		// tab content title
		$tabs_content_output .= '<div class="tabbed-chart__content--title">' . $tab_title . '</div>';

		// tab content chart wrap start
		$tabs_content_output .= '<div class="tabbed-chart__content--diagram"><div class="ct-chart ct-major-third ' . $chart_class . '"></div>';

		// if chart has more info
		$tabs_content_output .= ( $chart_moreinfo ) ? '<div class="tabbed-chart__content--diagram__info"><span>' . $chart_moreinfo . '</span></div>' : '';

		// tab content chart wrap end
		$tabs_content_output .= '</div>';

		// if chart has stats
		if( $chart_stats ) {
			// tab content stats start
			$tabs_content_output .= '<div class="tabbed-chart__content--stats">';

			// loop through stats
			foreach( $chart_stats as $cs ) {
				// stat : color
				$stats_color = ( isset( $chart->stats->colors[$stat_count] ) ) ? $chart->stats->colors[$stat_count] : '';
				// stat : label
				$stats_label = ( isset( $chart->stats->labels[$stat_count] ) ) ? $chart->stats->labels[$stat_count] : '';
				// stat : counter speed
				$stats_counterspeed = ( isset( $chart->stats->counterspeed ) ) ?  $chart->stats->counterspeed : '';

				// tab content stat output
				$tabs_content_output .= '<div class="tabbed-chart__content--stats__item" data-color="' . $stats_color . '"><span><small>' . $stats_label . '</small> <span class="counter" data-from="0" data-to="' . $cs . '" data-speed="' . $stats_counterspeed . '" data-refresh-interval="1">0</span>%</span></div>';

				// increase the stat count
				$stat_count++;
			};

			// tab content stats end
			$tabs_content_output .= '</div>';
		}

		// tab content end
		$tabs_content_output .= '</div>';

		// increase the tab count
		$tabcount++;
	}

	// end tabs output
	$tabs_output .= '</ul>';

	// include tabs output
	$output .= $tabs_output;

	// incluce tab content output
	$output .= $tabs_content_output;

	// include static actions output
	$output .= '<ul class="tabbed-chart__actions">'.
			'<li class="tabbed-chart__actions--item"><a href="#" class="tabbed-chart__actions--item__link"><i class="icon-upload-to-cloud"></i> <small>Upload Files</small></a></li>'.
			'<li class="tabbed-chart__actions--item"><a href="#" class="tabbed-chart__actions--item__link"><i class="icon-share"></i> <small>Share Link</small></a></li>'.
			'<li class="tabbed-chart__actions--item"><a href="#" class="tabbed-chart__actions--item__link"><i class="icon-back-in-time"></i> <small>Back Up</small></a></li>'.
		'</ul>'.
	'</div>';
}

// display output
echo $output;