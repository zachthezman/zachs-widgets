<?php
// copy the posts array to a php var
$posts = ( isset( $data->blogposts->posts ) ) ? $data->blogposts->posts : '';

// start output vars
$output = '';
$author_output = '';

// posts found
if( $posts ) {
	// listing posts
	foreach ( $posts as $post ) {
		// title
		$post_title = ( isset( $post->title ) ) ? $post->title : '';
		// image
		$post_image = ( isset( $post->image ) ) ? $post->image : '';

		// start output
		$output .= '<div class="widget blogpost widget-type--blogpost mod-width--one-fourth mod--shadow mod--rounded">';

		// if post image found
		$output .= ( $post_image ) ? '<div class="blogpost__featured-image"><img src="' . $post_image . '" alt="' . $post_title . '" title="' . $post_title . '"></div>' : '';

		// post author listed
		if( isset( $post->author ) ) {
			// author avatar
			$author_avatar = ( isset( $post->author->avatar ) ) ? $post->author->avatar : '';
			// author name
			$author_name = ( isset( $post->author->name ) ) ? $post->author->name : '';
			// author bio
			$author_bio = ( isset( $post->author->bio ) ) ? $post->author->bio : '';

			// start author output
			$author_output = '<div class="blogpost__author">';

			// if author avatar found
			$author_output .= ( $author_avatar ) ? '<div class="blogpost__author--avatar"><span class="blogpost__author--avatar__wrap"><img src="' . $author_avatar . '" alt="' . $author_name . '" title="' . $author_name . '"></span></div>' : '';

			// if author name and bio found
			if( $author_name || $author_bio ) {
				// continue author output
				$author_output .= '<div class="blogpost__author--info">';

				// if author name is found
				$author_output .= ( $author_name ) ? '<span class="blogpost__author--info__name">' . $author_name . '</span>' : '';

				// if author bio is found
				$author_output .= ( $author_bio ) ? '<span class="blogpost__author--info__bio">' . $author_bio . '</span>' : '';

				// end author name and bio output
				$author_output .= '</div>';
			}

			// end author output
			$author_output .= '</div>';
		}

		// include author output
		$output .= $author_output;

		// actions : post views
		$views = ( isset( $post->views ) ) ? $post->views : 0;
		// actions : post comments
		$comments = ( isset( $post->comments ) ) ? $post->comments : 0;
		// actions : post favorites
		$favorites = ( isset( $post->favorites ) ) ? $post->favorites : 0;

		// actions output
		$output .= '<div class="blogpost__actions--wrap">'.
			'<ul class="blogpost__actions">'.
				'<li class="blogpost__actions--item" title="Views"><a class="blogpost__actions--item__link color-hover--blue" href="#"><i class="icon-eye"></i> ' . $views . '</a></li>'.
				'<li class="blogpost__actions--item" title="Comments"><a class="blogpost__actions--item__link color-hover--green" href="#"><i class="icon-comment"></i> ' . $comments . '</a></li>'.
				'<li class="blogpost__actions--item" title="Favorites"><a class="blogpost__actions--item__link color-hover--red" href="#"><i class="icon-heart"></i> ' . $favorites . '</a></li>'.
			'</ul>'.
		'</div>';

		// end output
		$output .= '</div>';
	}
	// end listing posts
}

// display output
echo $output;