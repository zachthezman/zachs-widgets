<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<title>Zach's Widgets</title>

		<link rel="stylesheet" href="assets/css/zachs-widgets-font.css"><?php // include icon font styles ?>
		<link rel="stylesheet" href="assets/css/chartist.css"><?php // include Chartist styles ?>
		<link rel="stylesheet" href="assets/css/style.css"><?php // include main styles ?>

		<script type="text/javascript" src="assets/js/min/modernizr.min.js"></script><?php // include modernizr script ?>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script><?php // include jQuery ?>
		<script>window.jQuery || document.write("<script src=\"assets/js/min/jquery-2.1.4.min.js\"><\/script>")</script><?php // include local jQuery if CDN version is down ?>

		<?php // load Google Fonts ?>
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					families: ['Cabin:700', 'Open Sans:400,600,700']
				}
			});
		</script>
	</head>

	<body class="desktop">
		<h1 class="heading__title">Metro Vibes | Free Sample</h1>
		<h2 class="heading__subtitle">Get the full 100+ Elements on www.PixelKit.com</h2>