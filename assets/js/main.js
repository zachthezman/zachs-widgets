(function($){
	/**
	 * Touch Toggle for dropdown menus
	 * @param {text}   selector   this is the selector for the menu
	 */
	function TouchToggle(selector){
		var $document = $(document),
			$selector = $(selector),
			$activeClass = 'active';

		// selector states
		$selector.on({
			// on click
			"click" : function(e) {
				// check for active class
				if( $(this).hasClass($activeClass).toString() == 'false' ) {
					// stop the click
					e.preventDefault();
					// remove all active classes
					$selector.removeClass($activeClass);
					// find the selector parent and add active class
					$(this).parents($selector).addClass($activeClass);
					// add active class to selector
					$(this).addClass($activeClass);
					// return false to prevent click
					return false;
				}
			},
			// on mouseover
			"mouseover" : function(e) {
				// check for desktop class
				if( $('body').hasClass('desktop') ) {
					// add active class to selector
					$(this).addClass($activeClass);
				}
			},
			// on mouseout
			"mouseout" : function(e) {
				// check for desktop class
				if( $('body').hasClass('desktop') ) {
					// remove active class from selector
					$(this).removeClass($activeClass);
				}
			}
		});

		// if user presses a keyboard key
		$document.keyup(function(e) {
			// if the key is 'esc'
			if( e.keyCode == 27 ) {
				// remove active class from selector
				$(this).removeClass($activeClass);
			}
		});

		// if user stops the mouse click
		$document.mouseup(function(e) {
			if ( ! selector.is(e.target) && selector.has(e.target).length === 0 ){
				// remove active class from selector
				$(this).removeClass($activeClass).removeClass($activeClass);
			}
		});
	}

	/**
	 * Tab Charts for charts in tabs :)
	 * @param {json}   data   input JSON data
	 */
	function TabCharts(data,selector) {
		$(selector).each(function(i){
			var $tab = $(this),
				$tabID = $tab.data('tab'),
				$allTabs = $(selector),
				$tabContentID = $('[data-tab-content="' + $tabID + '"]'),
				$allTabsContent = $('[data-tab-content]'),
				$class_Current = 'current',
				$class_Show = 'mod--show',
				$class_Hide = 'mod--hide',
				$chartType = data.tabbed_charts.charts[i].options.type,
				$chartSeries = data.tabbed_charts.charts[i].options.series,
				$chartLabels = data.tabbed_charts.charts[i].options.labels,
				$chartClass = data.tabbed_charts.charts[i].options.class;

			// chart type is 'Pie' show this chart on page load
			if( $chartType === 'Pie' ) {
				// use Chartist plugin
				var $pie_chart = new Chartist.Pie('.' + $chartClass,
					// chart data
					{
						series: $chartSeries,
						labels: $chartLabels
					},
					// chart options
					{
						donut: true,
						showLabel: false,
						donutWidth: 50
					}
				);

				// start drawing chart
				$pie_chart.on('draw', function(data) {
					if(data.type === 'slice') {
						// Get the total path length in order to use for dash array animation
						var pathLength = data.element._node.getTotalLength();

						// Set a dasharray that matches the path length as prerequisite to animate dashoffset
						data.element.attr({
							'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
						});

						// Create animation definition while also assigning an ID to the animation for later sync usage
						var animationDefinition = {
							'stroke-dashoffset': {
								id: 'anim' + data.index,
								dur: 2000,
								from: -pathLength + 'px',
								to:  '0px',
								easing: Chartist.Svg.Easing.easeOutQuint,
								// We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
								fill: 'freeze'
							}
						};

						// If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
						if(data.index !== 0) {
							animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
						}

						// We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
						data.element.attr({
							'stroke-dashoffset': -pathLength + 'px'
						});

						// We can't use guided mode as the animations need to rely on setting begin manually
						// See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
						data.element.animate(animationDefinition, false);
					}
				});
			}

			// tab data swap starts here
			$tab.on('click',function(){
				// check for current class on tab
				if( ! $tab.hasClass($class_Current) ) {
					// remove all current classes from all tabs
					$allTabs.removeClass($class_Current);
					// add the current class to the clicked tab
					$tab.addClass($class_Current);
					// hide all current tab content blocks
					$allTabsContent.removeClass($class_Show).addClass($class_Hide);
					// show current tab content block | make the stat counters count
					$tabContentID.removeClass($class_Hide).addClass($class_Show).find('.counter').countTo();
				}

				// if chart type is 'Bar' show this chart after the tab has been clicked
				if( $chartType === 'Bar' ) {
					// use Chartist plugin
					new Chartist.Bar('.' + $chartClass,
						// chart data
						{
							series: $chartSeries,
							labels: $chartLabels
						},
						// chart options
						{
							high: 60,
							seriesBarDistance: 30,
							axisX: {
								showLabel: false,
								offset: 0
							},
							axisY: {
								showLabel: false,
								offset: 5,
								onlyInteger: true,
								labelInterpolationFnc: function(value) {
									return value + '%';
								},
								scaleMinSpace: 20
							}
						}
					);
				}
			});

			// if counters are present on load, make them count
			$('[data-tab-content].mod--show .counter').countTo();
		});

		// center up the chart info after the chart is present
		$('[class*="__info"]').css({ lineHeight : $('.ct-chart').height() + 'px' });
	}

	// start up the Touch Toggle for the main nav
	TouchToggle('.main-nav .has--dropdown');

	// Load up the JSON data for the charts
	$.ajax({
		url: "../../data.json",
		async: true,
		dataType: 'json',
		success: function(data) {
			TabCharts(data,'[data-tab]');
		}
	});

})(jQuery);