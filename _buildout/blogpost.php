<div class="blogpost widget-type--blogpost mod-width--one-fourth mod--shadow mod--rounded">
    <div class="blogpost__featured-image">
        <img src="http://placehold.it/370x204" alt="Blog Post" title="Blog Post">
    </div>
    <div class="blogpost__author">
        <div class="blogpost__author--avatar">
            <span class="blogpost__author--avatar__wrap"><img src="http://placehold.it/72x72" alt="Zach Abernathy" title="Zach Abernathy"></span>
        </div>
        <div class="blogpost__author--info">
            <span class="blogpost__author--info__name">Zach Abernathy</span>
            <span class="blogpost__author--info__bio">Your talent amazes! This is awesome. Excited to see the final product.</span>
        </div>
    </div>
    <div class="blogpost__actions--wrap">
        <ul class="blogpost__actions">
            <li class="blogpost__actions--item" title="Views"><a class="blogpost__actions--item__link color-hover--blue" href="#"><i class="icon-eye"></i> 172</a></li>
            <li class="blogpost__actions--item" title="Comments"><a class="blogpost__actions--item__link color-hover--green" href="#"><i class="icon-comment"></i> 34</a></li>
            <li class="blogpost__actions--item" title="Favorites"><a class="blogpost__actions--item__link color-hover--red" href="#"><i class="icon-heart"></i> 210</a></li>
        </ul>
    </div>
</div>