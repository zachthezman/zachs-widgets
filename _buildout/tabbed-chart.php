<div class="tabbed-chart mod--rounded">
    <ul class="tabbed-chart__tabs">
        <li class="tabbed-chart__tabs--item current" data-tab="diagram">Diagram Stats</li>
        <li class="tabbed-chart__tabs--item" data-tab="report">Month Report</li>
    </ul>
    <div data-tab-content="diagram" class="tabbed-chart__content mod--rounded mod--show">
        <div class="tabbed-chart__content--title">Data Transfer</div>
        <div class="tabbed-chart__content--diagram"><div class="ct-chart ct-major-third donut-chart"></div><div class="tabbed-chart__content--diagram__info"><span>2,435 files <b>47Gb</b></span></div></div>
        <div class="tabbed-chart__content--stats">
            <div class="tabbed-chart__content--stats__item" data-color="green"><span><small>Audio</small> <span class="counter" data-from="0" data-to="55" data-speed="1000" data-refresh-interval="1">0</span>%</span></div>
            <div class="tabbed-chart__content--stats__item" data-color="red"><span><small>Video</small> <span class="counter" data-from="0" data-to="28" data-speed="1000" data-refresh-interval="1">0</span>%</span></div>
            <div class="tabbed-chart__content--stats__item" data-color="yellow"><span><small>Photo</small> <span class="counter" data-from="0" data-to="17" data-speed="1000" data-refresh-interval="1">0</span>%</span></div>
        </div>
    </div>
    <div data-tab-content="report" class="tabbed-chart__content mod--rounded mod--hide">
        <div class="tabbed-chart__content--title">Total Usage</div>
        <div class="tabbed-chart__content--diagram"><div class="ct-chart ct-major-third bar-chart"></div></div>
        <div class="tabbed-chart__content--stats">
            <div class="tabbed-chart__content--stats__item" data-color="green"><span><small>Audio</small> <span class="counter" data-from="0" data-to="58" data-speed="500" data-refresh-interval="1">0</span>%</span></div>
            <div class="tabbed-chart__content--stats__item" data-color="red"><span><small>Video</small> <span class="counter" data-from="0" data-to="31" data-speed="500" data-refresh-interval="1">0</span>%</span></div>
            <div class="tabbed-chart__content--stats__item" data-color="yellow"><span><small>Photo</small> <span class="counter" data-from="0" data-to="16" data-speed="500" data-refresh-interval="1">0</span>%</span></div>
        </div>
    </div>
    <ul class="tabbed-chart__actions">
        <li class="tabbed-chart__actions--item"><a href="#" class="tabbed-chart__actions--item__link"><i class="icon-upload-to-cloud"></i> <small>Upload Files</small></a></li>
        <li class="tabbed-chart__actions--item"><a href="#" class="tabbed-chart__actions--item__link"><i class="icon-share"></i> <small>Share Link</small></a></li>
        <li class="tabbed-chart__actions--item"><a href="#" class="tabbed-chart__actions--item__link"><i class="icon-back-in-time"></i> <small>Back Up</small></a></li>
    </ul>
</div>