<nav class="main-nav mod--shadow">
    <ul class="main-nav__list mod--rounded">
        <li class="main-nav__list--item has--dropdown"><a href="#" class="main-nav__list--item__link"><i class="icon-mapmarker"></i> <span data-alert="3">Check In</span></a>
            <ul class="main-nav__dropdown">
                <li class="main-nav__dropdown--item"><a href="#" class="main-nav__dropdown--item__link">Dropdown Item 1</a></li>
                <li class="main-nav__dropdown--item"><a href="#" class="main-nav__dropdown--item__link">Dropdown Item 2</a></li>
                <li class="main-nav__dropdown--item"><a href="#" class="main-nav__dropdown--item__link">Dropdown Item 3</a></li>
                <li class="main-nav__dropdown--item"><a href="#" class="main-nav__dropdown--item__link">Dropdown Item 4</a></li>
            </ul>
        </li>
        <li class="main-nav__list--item"><a href="#" class="main-nav__list--item__link"><i class="icon-heart"></i> <span>Events</span></a></li>
        <li class="main-nav__list--item"><a href="#" class="main-nav__list--item__link"><i class="icon-user"></i> <span>Account</span></a></li>
        <li class="main-nav__list--item"><a href="#" class="main-nav__list--item__link"><i class="icon-cog"></i> <span>Settings</span></a></li>
    </ul>
</nav>