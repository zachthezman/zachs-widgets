<div class="profile mod--rounded mod--shadow">
    <div class="profile__user">
        <div class="profile__user--avatar">
            <span class="profile__user--avatar__wrap"><img src="http://placehold.it/72x72" alt="Zach Abernathy" title="Zach Abernathy"></span>
        </div>
        <div class="profile__user--info">
            <span class="profile__user--info__name">Zach <br>Abernathy</span>
            <span class="profile__user--info__bio">15,323 followers</span>
        </div>
    </div>
    <div class="profile__actions--wrap">
        <ul class="profile__actions">
            <li class="profile__actions--item"><a class="profile__actions--item__link" href="#"><i class="icon-user"></i> Edit user</a></li>
            <li class="profile__actions--item current"><a class="profile__actions--item__link" href="#"><i class="icon-stats-bars"></i> Web statistics</a></li>
            <li class="profile__actions--item"><a class="profile__actions--item__link" href="#"><i class="icon-cog"></i> Upload settings</a></li>
            <li class="profile__actions--item"><a class="profile__actions--item__link" href="#"><i class="icon-heart"></i> Events</a></li>
        </ul>
    </div>
</div>