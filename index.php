<?php include_once('inc/header.php'); ?>

<?php
	// copy file content into a string var
	$data_file = file_get_contents('data.json');

	// convert the string to a json object
	$data = json_decode($data_file);
?>

<?php include_once('inc/widgets/widget-navigation.php'); ?>

<?php include_once('inc/widgets/widget-blogpost.php'); ?>

<?php include_once('inc/widgets/widget-tabbed-chart.php'); ?>

<?php include_once('inc/widgets/widget-profile.php'); ?>

<?php include_once('inc/footer.php'); ?>